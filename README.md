### What is it

Library for generating native Word table XML.

### How to get set up

Get the pandoc python filter, which enables raw blocks in markdown, from here:

<https://gist.github.com/bpj/e6e53cbe679d3ec77e25>

A copy of the pandoc filter plugin is also in the installation dir of this package. You could find the path by `path.package('WordTable')`.

You need a couple of python libraries.

``` shell
sudo pip install pandocfilters pandoc-attributes
```

Install this package in R

``` r
library(devtools)
install_bitbucket('capia_as/wordtable')
```

Enable python raw code plugin as pandoc filter in your RMarkdown

``` r
---
title: "WordTableTest"
output: 
  word_document: 
    pandoc_args: [ 
      "--filter", "./pandoc-code2raw.py" 
    ]
---
    ```{r, echo = FALSE, results = "asis"}
    library(WordTable)
    word_table(cars)
    ```
```

### Examples

``` r
word_table(data, include.colnames = FALSE) %>%
  add_header_row(c('Species', 'Length', 'Width', 'Length', 'Width')) %>%
  add_header_row(c('','Sepal', 'Petal'), col_span = c(1,2,2)) %>%
  set_ratios(c(.4, .2, .2, .2, .2)) %>% # Adds up to one (100%)
  column_number_format(2:5, nsmall = 1) %>%
  set_theme(light_theme)
```

### Issues and limitations

THIS IS AN EARLY VERSION! Very few checks and errors are implemented so far. No tests are made.

#### Works

-   Straight data frames to tables
-   Extra headers
-   Merge columns in extra headers
-   Auto detects numerical columns and right adjusts those
-   Left adjusts text columns
-   Headers are properly tagged, so they will repeat on top of every page for long tables
-   Styling
-   Custom borders

#### Not yet implemented

-   Apply noWrap attribute to numeric cells.
-   row cantSplit attribute can be interesting in hierarchical tables. Split only on root change for instance.
-   Landscape tables on own page.
-   Row merging
-   Column group regions. Odd/even colgroups for styling.
-   No hierarchy &lt;- data.tree can be used.
-   No smart column widths &lt;- in the works with a rudimentary version.
-   Table caption &lt;- Couldn't make it work in open office...
-   Shading
-   Cell spacing

#### About styling

Styling is implemented in theme class objects.

``` r
light_theme = wt_theme(
  header.bottom = wt_style(
    border_bottom = wt_border(
      color = '#000000'
    )
  ),
  odd.row = wt_style(
    fill_color = '#DDDDDD'
  )
)
```

Many available regions to style.

### Way it works

When generating html files from RMarkdown, you are able to pass through raw html without much problem. Same goes for raw latex to pdf conversion. The same principles does not apply for pandocs implementation of the word format. All raw xml will be stripped out by pandoc. By installing the `pandoc-code2raw.py` filter, we can encapsulate raw word xml in code chunks marked as `{raw=openxml}`. This way pandoc won't touch it, and it will get passed through from rendered markdown to final word document. Bonus is that it will get stripped out if outputting to other format, so we can retain some output agnosticity in the Rmarkdown.

    ```{raw=openxml}
    <w:tbl>...</w:tbl>
    ```

Luckily this seems to be solved in upcoming version of pandoc: <https://github.com/jgm/pandoc/issues/3537> New format seems to be:

        ```{=openxml}
        <w:tbl>...</w:tbl>
        ```

Lots of help from this page on openxml standard: <http://officeopenxml.com/WPtable.php>
