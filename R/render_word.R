
#fable_render.docx <- function(ftable, ...) {
print.fable_render_docx <- function(renderer) {
  ftable <- renderer$ftable
  tbl_style <- .tbl_style(ftable)
  #ftable$region_map <- .generate_region_map(ftable)
  ftable$style_map <- .generate_style_map(ftable)
  output <- paste0("\n```{raw=openxml}\n")
  #output %.=% tbl_style$cf
  output %.=% paste0('<w:tbl>
    <w:tblPr>
      <w:tblW w:w=',ftable$width,' w:type="dxa"/>',
                     ftable$caption,
                     tbl_style$style,
                     '</w:tblPr>')
  output %.=% paste0('<w:tblGrid>', .tbl_grid(ftable), '</w:tblGrid>')
  output <- paste0(output, .header(ftable))
  output <- paste0(output, .body(ftable))
  output %.=% '</w:tbl><w:p>
            <w:pPr>
                <w:pStyle w:val="Normal"/>
                <w:rPr></w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr></w:rPr>
            </w:r>
        </w:p>\n```\n'
  output
}

fable_render_docx <- function(ftable) {
  #structure(list(), class = c('fable_render', 'html'))
  structure(class = 'fable_render_docx',
            list(
              ftable = ftable
            )
  )
}

#' Get merged style, based on all style rules that affects a given cell.
#'
#' @param wt
#' @param row_idx
#' @param col_idx
#'
#' @return
#' @export
#'
#' @examples
.get_cell_style <- function(wt, row_idx, col_idx) {
  .cell_style( wt$style_map[[row_idx, col_idx]])
}

#' Get width of cell
#' Affected by custom set ratios and/or colspans.
#'
#' @param wt
#' @param col_index
#' @param col_span
#'
#' @return
#' @export
#'
#' @examples
.get_cell_width <- function(wt, col_index, col_span) {
  width <- round( sum( wt$col_widths[ col_index : (col_index + col_span - 1)]))
  grid_span <- if(col_span > 1) sprintf('<w:gridSpan w:val="%d"/>', col_span) else ''
  cell_width <- sprintf('<w:tcW w:w ="%s" w:type="dxa"/>', width)
  paste(grid_span, cell_width, collapse = "\n")
}

#' Renders the header part of the table
#'
#' @param wt
#'
#' @return
#' @export
#'
#' @examples
.header <- function(wt) {
  header <- ''
  for(r in seq_len(wt$header_height)) {
    row <- wt$headers[[(wt$header_height-(r-1))]] #header rows are pushed at ent of list, need to read in reverse.
    cell_spacing <- ''
    if(row[['cell_spacing']] > 0) {
      cell_spacing <- sprintf('<w:tblCellSpacing w:w="%s" w:type="dxa"/>', row[['cell_spacing']])
    }
    header %.=% paste0('<w:tr><w:trPr><w:tblHeader/>',cell_spacing,'</w:trPr>')
    for(c in seq_along(row$text)) {
      #str(row$text[c])
      header <- paste0(header, .cell(wt, row$text[c], r, c, row$alignment[c], row$col_span[c]))
    }
    header %.=% '</w:tr>'
  }
  header
}

#' Renders the body section of the table
#'
#' @param wt
#'
#' @return
#' @export
#'
#' @examples
.body <- function(wt) {
  body <- ''
  for(r in seq_len(wt$num_rows)) {
    body %.=% '<w:tr><w:trPr><w:cantSplit/></w:trPr>'
    for(c in seq_len(wt$num_cols)) {
      if(is.na(wt$col_spans[r,c])) {
        #This cell i merged over by col span - ignore
        next
      }
      no_wrap = FALSE
      if(wt$is_numeric[c]) {
        cell_value <- .format_number(wt, wt$data[r,c], c)
        no_wrap = TRUE
      } else if (wt$is_factor[c]) {
        cell_value <- as.character(wt$data[r,c])
      } else {
        cell_value <- wt$data[r,c]
      }
      body <- paste(body, .cell(wt,
                                cell_value,
                                row_idx = r + wt$header_height,
                                col_idx = c,
                                align = wt$aligns[c],
                                col_span = wt$col_spans[r,c],
                                no_wrap = no_wrap)
      )
    }
    body %.=% '</w:tr>'
  }
  body
}

#' Renders an individual cell
#'
#' @param wt
#' @param cell_value
#' @param row_idx
#' @param col_idx
#' @param align
#' @param col_span
#'
#' @return
#' @export
#'
#' @examples
.cell <- function(wt, cell_value, row_idx, col_idx, align = 'left', col_span = 1, no_wrap = FALSE) {
  cell_width <- .get_cell_width(wt, col_idx, col_span)
  cell_style <- .get_cell_style(wt, row_idx , col_idx)
  paste0('<w:tc>
         <w:tcPr>
         ',cell_width,'
         ',cell_style$tcPr,'
         ',ifelse(no_wrap, '<w:noWrap/>', ''),'
         </w:tcPr>
         <w:p>
         <w:pPr>
         <w:jc w:val="',align,'"/>
         <!--<w:pStyle w:val="Normal"/>-->
         <!--<w:spacing w:before="',wt$spacing$before,'" w:after="',wt$spacing$after,'"/>-->
         ',cell_style$pPr,'
         </w:pPr>',
         #.render_cell_note(wt, row_idx, col_idx),
         '<w:r>
         <w:rPr>
         ',cell_style$rPr,'
         </w:rPr>
         <w:t>',cell_value,'</w:t>
         </w:r>
         </w:p>
         </w:tc>')
}

.cell_margin <- function(wts) {
  output <- ''
  has_margin <- FALSE
  # use start and end vs. left right or both. left right deprecated, but is what open office understands...
  for(i in c('top','bottom','left','right')) {
    j <- paste0('margin_', i)
    if(!is.na(wts[[j]])) {
      output %.=% sprintf('<w:%s w:w="%s" w:type="dxa"/>', i, wts[[j]])
      has_margin <- TRUE
    }
  }
  ifelse(has_margin, paste0('<w:tcMar>', output, '</w:tcMar>'), '')
}

.paragraph_spacing <- function(wts) {
  before <- ifelse(is.na(wts$paragraph_spacing_before), 120, wts$paragraph_spacing_before)
  after <- ifelse(is.na(wts$paragraph_spacing_after), 120, wts$paragraph_spacing_after)
  if(is.na(wts$paragraph_spacing_before) && is.na(wts$paragraph_spacing_after)) {
    output <- ''
  } else {
    output <- sprintf('<w:spacing w:before="%s" w:after="%s" w:beforeAutospacing="0" w:afterAutospacing="0"/>',
                      before,
                      after
    )
  }
  output
}

.fix_color_code <- function(color_code) {
  gsub("#", "", color_code)
}

.cell_style <- function(wts) {
  #stop(str(wts))
  #border <- border_style(wts)
  list(
    # Cell properties. Background and border stuff.
    tcPr = paste0(
      .cell_border(wts),
      .cell_margin(wts),
      ifelse(is.na(wts$vertical_align), '<w:vAlign w:val="center"/>', sprintf('<w:vAlign w:val="%s"/>', wts$vertical_align)),
      ifelse(is.na(wts$fill_color), '', sprintf('<w:shd w:val="clear" w:color="auto" w:fill="%s"/>', .fix_color_code(wts$fill_color))),
      # <w:noWrap/>
      #<w:tcFitText/>
      ''
    ),
    pPr = paste0(
      .paragraph_border(wts),
      .paragraph_spacing(wts)
    ),
    # Running paragraph properties. Font stuff.
    rPr = paste0(
      ifelse(is.na(wts$bold), '', '<w:b/>'),
      ifelse(is.na(wts$italic), '', '<w:i/>'),
      ifelse(is.na(wts$underline), '', '<w:u w:val="single"/>'),
      ifelse(is.na(wts$strike), '', '<w:strike/>'),
      ifelse(is.na(wts$font_color), '', sprintf('<w:color w:val="%s" />', .fix_color_code(wts$font_color))),
      ifelse(is.na(wts$font_size), '', sprintf('<w:sz w:val="%d"/>', wts$font_size)),
      ifelse(is.na(wts$font), '', sprintf('<w:rFonts w:ascii="%s" w:hAnsi="%s"/>', wts$font, wts$font))
    )
  )
}

#' Renders the table grid definition
#'
#' @param wt
#'
#' @return
#' @export
#'
#' @examples
.tbl_grid <- function(wt) {
  paste(sapply(wt$col_widths, function(col_width) {
    sprintf('<w:gridCol w:w="%f"/>', col_width)
  }), collapse = "\n")
}

#' Get style id tag for table, in case we shall inherit from template document.
#'
#' @param wt
#'
#' @return
#' @export
#'
#' @examples
.tbl_style <- function(wt) {
  tbl_style <- list(cf = '', style = '')
  #if(is(wt$conditional_formatting, 'conditional_formatting')) {
  #  tbl_style$cf <- .conditional_formatting(wt$conditional_formatting)
  #}
  if(!is.na(wt$style_id)) {
    tbl_style$style <- sprintf('<w:tblStyle w:val="%s"/>', wt$style_id)
  }
  tbl_style
}

.cell_border <- function(wts) {
  output <- ''
  has_border <- FALSE
  for(i in c('top','right','bottom','left')) {
    j <- paste0('border_', i)
    if(is(wts[[j]], 'fable_border')) {
      color <- .fix_color_code(wts[[j]]$color)
      output %.=% sprintf('<w:%s w:val="%s" w:sz="%d" w:space="%d" w:color="%s"/>', i, wts[[j]]$style, wts[[j]]$width, wts[[j]]$space, color)
      if(i %in% c('right','left')) {
        output %.=% sprintf('<w:innerV w:val="%s" w:sz="%d" w:space="%d" w:color="%s"/>', wts[[j]]$style, wts[[j]]$width, wts[[j]]$space, color)
      } else {
        output %.=% sprintf('<w:innerH w:val="%s" w:sz="%d" w:space="%d" w:color="%s"/>', wts[[j]]$style, wts[[j]]$width, wts[[j]]$space, color)
      }
      has_border <- TRUE
    }
  }
  ifelse(has_border, paste0('<w:tcBorders>', output, '</w:tcBorders>'), '')
}

.paragraph_border <- function(wts) {
  output <- ''
  has_border <- FALSE
  for(i in c('top','right','bottom','left')) {
    j <- paste0('paragraph_border_', i)
    if(is(wts[[j]], 'fable_border')) {
      color <- .fix_color_code(wts[[j]]$color)
      output %.=% sprintf('<w:%s w:val="%s" w:sz="%d" w:space="%d" w:color="%s"/>', i, wts[[j]]$style, wts[[j]]$width, wts[[j]]$space, color)
      has_border <- TRUE
    }
  }
  ifelse(has_border, paste0('<w:pBdr>', output, '</w:pBdr>'), '')
}


pagebreak <- function() {
  return ("\n```{raw=openxml}
    <w:p>
      <w:r>
        <w:br w:type='page'/>
      </w:r>
    </w:p>\n```\n")
}

pagebreak_landscape <- function() {
  return ("\n```{raw=openxml}
    <w:sectPr>
      <w:pgSz w:w='15840' w:h='12240' w:orient='landscape'/>
    </w:sectPr>
    <w:p>
      <w:r>
        <w:br w:type='page'/>
      </w:r>
    </w:p>\n```\n")
}
