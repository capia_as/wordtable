measure_unit <- function(value, dpi = 150) {
  mu <- structure(class = "measure_unit", list(
    value = NA,
    unit = NA,
    dpi = dpi,
    supported_units = c('twips', 'in', 'px', 'cm', 'mm'),
    print_unit = c(FALSE, TRUE, TRUE, TRUE, TRUE),
    rel_twips = c(1, 1/1440, 1/(1440/dpi), 0.0017639, 0.00017639)
  ))
  names(mu$print_unit) <- mu$supported_units
  names(mu$rel_twips) <- mu$supported_units

  if(is.numeric(value)) {
    # Word supports only twips, and uses no unit.
    mu$value <- value
    mu$unit <- 'twips'
  } else {
    # Matching input to value and unit
    # Trying named capture groups, but doesn't do anything.
    m <- regexec('^(?<value>\\d+)(?<unit>\\w+)$', value, perl = TRUE)
    if(length(m[[1]] == 3)) {
      r <- regmatches(value, m)[[1]]
      # Since named capture groups doesn't work :(
      names(r) <- c('full','value','unit')
      if(r['unit'] %in% mu$supported_units) {
        mu$value <- as.numeric(r['value'])
        mu$unit <- r['unit']
      } else {
        stop(sprintf('Not supported unit: %s', r['unit']))
      }
    } else {
      stop(sprintf('Does not recognise "%s" as a measure', value))
    }
  }
  mu
}

toString.measure_unit <- function(mu, ...) {
  if(mu$print_unit[mu$unit]) {
    paste0(ceiling(mu$value), mu$unit)
  } else {
    as.numeric(ceiling(mu$value))
  }
}

print.measure_unit <- function(mu, ...) {
  print(toString(mu))
}

format.measure_unit <- function (mu, target_unit, ...) {
  if(!target_unit %in% mu$supported_units) {
    stop(sprintf('Target unit "%s" not supported', target_unit))
  }
  mu$value <- mu$value / mu$rel_twips[mu$unit] * mu$rel_twips[target_unit]
  mu$unit <- target_unit
  toString(mu)
}
