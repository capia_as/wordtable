# <w:p>
#   <w:pPr>
#     <w:pStyle w:val="Table"/>
#     <w:keepNext/>
#     <w:rPr/>
#   </w:pPr>
#   <w:r>
#     <w:rPr/>
#     <w:t xml:space="preserve">Table </w:t>
#   </w:r>
#   <w:r>
#     <w:rPr/>
#     <w:fldChar w:fldCharType="begin"/>
#   </w:r>
#   <w:r>
#     <w:instrText> SEQ Table \\* ARABIC </w:instrText>
#   </w:r>
#   <w:r>
#     <w:fldChar w:fldCharType="separate"/>
#   </w:r>
#   <w:r>
#     <w:t>1</w:t>
#   </w:r>
#   <w:r>
#     <w:fldChar w:fldCharType="end"/>
#   </w:r>
#   <w:r>
#     <w:rPr/>
#     <w:t>: Table dexcription</w:t>
#   </w:r>
# </w:p>

caption <- function(wt, caption_text, position = 'top', sequence = TRUE, numeric_style = 'arabic') {
  wt$caption <- caption_text
  wt
}

title <- function(wt, title_text) {
  wt$title <- title_text
  wt
}

add_note <- function(wt, note, cell_location = NA, symbol = NA) {
  symbol <- ifelse(is.na(symbol), wt$note_sequence, symbol)
  if(!is.na(cell_location)) {
    wt$note_map[[cell_location]] <- list(
      symbol = symbol,
      sequence = wt$note_sequence
    )
  }
  wt$notes[wt$note_sequence] <- list(
    symbol = symbol,
    note = note
  )
  wt$note_sequence <- wt$note_sequence + 1
  wt
}

.generate_note_map <- function(wt) {
  table_dim <- .table_dim(wt)
  note_map <- vector('list', table_dim$table_height * table_dim$table_width)
  dim(note_map) <- c(table_dim$table_height, table_dim$table_width)
  note_map
}

#' Renders the symbol as superscript in front of cell content.
#' Real footnottes are difficult to do, because notes are stored in separate file.
.render_cell_note_reference <- function(wt, row, col) {
  if(is(wt$note_map[[row,col]], 'list')) {
    note <- wt$note_map[[row,col]]
    symbol <- ifelse(is.na(note$symbol), note$sequence, note$symbol)
    # Check for symbol, and use it
    # If no symbol, use sequence.
    paste0('<w:r>
            <w:rPr>
              <w:rStyle w:val="FootnoteAnchor"/>
              <!--<w:sz w:val="28"/>-->
              <w:vertAlign w:val="superscript"/>
              <w:i/>
            </w:rPr>
              <!--<w:footnoteReference w:id="2"/>-->
              <w:t>',symbol,'</w:t>
          </w:r>')
  }
}

#' @rant Npt sure is each note belongs in one paragraph, or multiple with breaks between them...
#' Cleaner, easier to produce inside each paragraph
#' Should probably avoid page break between them.
#' Sould i add <w:keepNext/> on every p but the last?
#' Wrap table and notes in a section?
.render_notes <- function(wt) {
  if(length(wt$notes) == 0) {
    return ('')
  }
  Reduce(function(note) {
    paste0('<w:p>
        <w:pPr>
          <w:spacing w:before="60" w:after="60"/>
          <w:pStyle w:val="Footnote"/>
        </w:pPr>
        </w:pPr>
        <w:r>
          <w:rPr>
            <w:rStyle w:val="FootnoteAnchor"/>
            <!--<w:sz w:val="28"/>-->
            <w:vertAlign w:val="superscript"/>
            <w:i/>
          </w:rPr>
          <w:t xml"space="preserve">',note$symbol,' </wt>
        </w:r>
        <w:r>
          <w:rPr>
            <!--<w:sz w:val="28"/>-->
          </w:rPr>
          <w:t xml"space="preserve">',note$note,':</wt>
        </w:r>
      </w:p>')
  }, wt$notes)
}

.render_title <- function(wt) {
  if(is.na(wt$title)) {
    return ('')
  }
  paste0('<w:p>
      <w:pPr>
        <w:pStyle w:val="TableHeading"/>
      </w:pPr>
      <w:r>
        <w:t>',wt$title,'</w:t>
      </w:r>
    </w:p>')
}

#' @todo sequence prefix. Default "Table"
#' @todo sequence numbering
#' @todo cleanup markup
.render_caption <- function(wt) {
  if(is.na(wt$caption)) {
    return ('')
  }
  paste0('<w:p>
    <w:pPr>
      <w:pStyle w:val="Table"/>
      <w:keepNext/>
      <w:rPr/>
    </w:pPr>
  <w:r>
    <w:rPr/>
    <w:t xml:space="preserve">Table </w:t>
  </w:r>
  <w:r>
    <w:rPr/>
    <w:fldChar w:fldCharType="begin"/>
  </w:r>
  <w:r>
    <w:instrText> SEQ Table \\* ARABIC </w:instrText>
  </w:r>
  <w:r>
    <w:fldChar w:fldCharType="separate"/>
  </w:r>
  <w:r>
    <w:t>',wt$caption_seq_number,'</w:t>
  </w:r>
  <w:r>
    <w:fldChar w:fldCharType="end"/>
  </w:r>
  <w:r>
    <w:rPr/>
    <w:t>: ',wt$caption,'</w:t>
  </w:r>
</w:p>')
}
