#' @todo Align with default auto ?
fable_style <- function(font_color = NA,
                     fill_color = NA,
                     font_size = NA,
                     bold = NA,
                     italic = NA,
                     underline = NA,
                     strike = NA,
                     font = NA,
                     vertical_align = NA,
                     border_top = NA,
                     border_right = NA,
                     border_bottom  = NA,
                     border_left = NA,
                     border_insideH = NA,
                     border_insideV = NA,
                     paragraph_border_top = NA,
                     paragraph_border_right = NA,
                     paragraph_border_bottom = NA,
                     paragraph_border_left = NA,
                     paragraph_spacing_before = NA,
                     paragraph_spacing_after = NA,
                     margin_top = NA,
                     margin_right = NA,
                     margin_bottom = NA,
                     margin_left = NA,
                     ...) {
  if(length(list(...)) > 0) {
    str(list(...))
    stop('This is not a recognized style attribute')
  }
  elements <- as.list(environment())
  #if(!is(elements$border, 'fable_border')) {
  #  stop('Border is not correct type')
  #}
  structure(class = "fable_style", elements)
}

fable_style.print <- function(fable_style) {
  Filter(Negate(`is.na`), fable_style)
}

#' Merge styles giving precedence to right hand side unless NA
#' Make work for theme AND styles?
#' @param lhs
#' @param rhs
#'
#' @return
#' @export
#'
#' @examples
`%merge%` <- function(lhs, rhs) {
  .r <- list()
  for(.i in names(rhs)) {
    # If no style in right had side
    if(length(rhs[[.i]]) == 0 || is.na(rhs[[.i]])) {
      # We use left hand side
      .r[[.i]] <- lhs[[.i]]
    } else {
      # Otherwise we use right hand side
      .r[[.i]] <- rhs[[.i]]
    }
  }
  .r
}


#' Factory for merging list of styles
#' Belongs to theme?
#'
#' @param styles list List of fable_styles
#'
#' @return fable_style
#' @export
#'
#' @examples
.style_merge <- function(styles) {
  #' Gets a list result of merged styles
  #' @todo Order styles in correct order first. Probably in order of fable_theme attributes.
  merged_style <- Reduce(`%merge%`, styles)
  # return this list as a fable_style object
  do.call(fable_style, as.list(merged_style))
}
