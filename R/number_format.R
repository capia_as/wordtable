#' Set a number formatter for given columns
#'
#' @param wt
#' @param column Can be number "3", vector "c(3,5") or range "3:6".
#' @param divide Divide number by this dividend to display values in 1000 f.ex.
#' @param nsmall Fixed number of decimal digits
#' @param digits Max digits
#' @param prefix
#' @param suffix
#' @param big.mark Thousand separator
#' @param decimal.mark
#'
#' @return
#' @export
#'
#' @examples
column_number_format <- function (wt, column, divide = 1, nsmall = 0, digits = 0, prefix = "", suffix = "", big.mark = " ", decimal.mark = ",") {
  for(i in column) {
    # For loop hack as I coldn't assign a single value to multiple list entries at once through ex. list[2:4] <- function() {}
    wt$col_number_format[[i]] <- .number_format_closure(divide, nsmall, digits, prefix, suffix,  big.mark, decimal.mark)
  }
  wt
}

#' Helper function that returns a preset number formatter as a closure function
.number_format_closure <- function(divide = 1, nsmall = 0, digits = 0,
                              prefix = "", suffix = "",
                              big.mark = " ", decimal.mark = ",") {
  # Return a closure function
  function(x, ...) {
    if(is.nan(x) || is.na(x) || is.infinite(x)) {
      return ('')
    }
    paste0(prefix,
           format(x/divide, ...,
                  big.mark = big.mark,
                  decimal.mark = decimal.mark,
                  digits = digits,
                  nsmall = nsmall,
                  scientific = FALSE,
                  trim = TRUE),
           suffix)
  }
}

#' Formats a cell value according to assigned number formatter by column
.format_number <- function(wt, value, col_index) {
  if(is.null(wt$col_number_format[[col_index]])) {
    # No formatter, return value as is
    value
  } else {
    # Run through assigned formatter
    wt$col_number_format[[col_index]](value)
  }
}
